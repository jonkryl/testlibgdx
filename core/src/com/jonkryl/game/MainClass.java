package com.jonkryl.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class MainClass extends ApplicationAdapter implements GestureDetector.GestureListener {
    private SpriteBatch batch;
    private Sprite sprite;
    private OrthographicCamera camera;

    private float ration;

    @Override
    public void create() {
        batch = new SpriteBatch();
        sprite = new Sprite(new Texture(Gdx.files.internal("drky.jpg")));
        sprite.setPosition(-sprite.getWidth() / 2, -sprite.getHeight() / 2);
        sprite.setCenter(0.5f, 0.5f);
        ration = sprite.getWidth() / sprite.getHeight();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.update();

        GestureDetector gestureDetector = new GestureDetector(this);
        Gdx.input.setInputProcessor(gestureDetector);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        sprite.draw(batch);
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        Vector3 touchPos = new Vector3(x, y, 0);
        Vector2 trueTouchPos = new Vector2(touchPos.x - (Gdx.graphics.getWidth() >> 1), (Gdx.graphics.getHeight() - touchPos.y) - (Gdx.graphics.getHeight() >> 1));

        if (trueTouchPos.x >= sprite.getX() && trueTouchPos.x <= sprite.getX() + sprite.getWidth() && trueTouchPos.y >= sprite.getY() && trueTouchPos.y <= sprite.getY() + sprite.getHeight()) {
            camera.unproject(touchPos);
            sprite.setPosition(touchPos.x - sprite.getWidth() / 2, touchPos.y - sprite.getHeight() / 2);
            checkInScreen();
        }
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        sprite.setSize(distance * ration, distance);
        return true;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        float deltaX = pointer2.x - pointer1.x;
        float deltaY = pointer2.y - pointer1.y;

        float angle = (float) Math.atan2((double) deltaY, (double) deltaX) * MathUtils.radiansToDegrees;

        angle += 90f;

        if (angle < 0)
            angle = 360f - (-angle);

        sprite.setRotation(-angle);
        sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2); // из-за этого изоброжение прыгает после поворота
        return true;
    }

    @Override
    public void pinchStop() {

    }

    private void checkInScreen() {
        if (sprite.getX() < -Gdx.graphics.getWidth() >> 1) {
            sprite.setX(-Gdx.graphics.getWidth() / 2);
        }
        if (sprite.getX() + sprite.getWidth() > Gdx.graphics.getWidth() >> 1) {
            sprite.setX((Gdx.graphics.getWidth() >> 1) - sprite.getWidth());
        }
        if (sprite.getY() < -Gdx.graphics.getHeight() >> 1) {
            sprite.setY(-Gdx.graphics.getHeight() / 2);
        }
        if (sprite.getY() + sprite.getHeight() > Gdx.graphics.getHeight() >> 1) {
            sprite.setY((Gdx.graphics.getHeight() >> 1) - sprite.getHeight());
        }
    }
}